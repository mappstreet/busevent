package com.izakos.mybus;

/**
 * Created by ovadia on 10.1.2018.
 */

public class EventMessage {
    public int flag;
    public Object objects;

    public EventMessage(int flag, Object objects) {
        this.flag = flag;
        this.objects = objects;
    }

    public EventMessage() {
    }
}
