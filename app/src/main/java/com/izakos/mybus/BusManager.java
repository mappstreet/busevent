package com.izakos.mybus;

import com.squareup.otto.Bus;

/**
 * Created by ovadia on 10.1.2018.
 */

public class BusManager {

    public static Bus instance;

    public static Bus getInstance(){
        if(instance == null){
            instance = new Bus();
        }
        return instance;
    }

    public static void setInstance(Bus instance) {
        BusManager.instance = instance;
    }
}
